# Scandinavian keyboard layouts

Carpalx and SWORD optimized Scandinavian layouts

## [Norto layouts](https://lykt.xyz/skl/norto/)

[Norwegian](https://lykt.xyz/skl/norto/#nb), [Danish](https://lykt.xyz/skl/norto/#da) and [Swedish](https://lykt.xyz/skl/norto/#sv) ortholinear SWORD optimizations.

## [Kvikk layouts](https://lykt.xyz/skl/kvikk/)

[Norwegian](https://lykt.xyz/skl/kvikk/#nb), [Danish](https://lykt.xyz/skl/kvikk/#da) and [Swedish](https://lykt.xyz/skl/kvikk/#sv) optimizations.

## [Skarp layouts](https://lykt.xyz/skl/skarp/)

[Norwegian](https://lykt.xyz/skl/skarp/#nb), [Danish](https://lykt.xyz/skl/skarp/#da) and [Swedish](https://lykt.xyz/skl/skarp/#sv) optimizations with standard home keys.

## [JASF layout](https://lykt.xyz/skl/jasf/)

Scandinavian optimization with standard home keys.
